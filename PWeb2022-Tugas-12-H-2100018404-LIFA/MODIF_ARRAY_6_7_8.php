<?php
$arrTinggi = array ("YUMI" => 170, "ALYA" => 160, "NAYA" => 168,"LIFA" => 155);
echo "<b>Array sebelum pengurutan</b>";
echo "<pre>";
print_r($arrTinggi);
echo "</pre>";

sort($arrTinggi);
reset($arrTinggi);
echo "<b>Array setelah pengurutan dengan sort()</b>";
echo "<pre>";
print_r($arrTinggi);
echo "</pre>";

rsort($arrTinggi);
reset($arrTinggi);
echo "<b>Array setelah pengurutan dengan rsort()</b>";
echo "<pre>";
print_r($arrTinggi);
echo "</pre>";

asort($arrTinggi);
reset($arrTinggi);
echo "<b>Array setelah pengurutan dengan asort()</b>";
echo "<pre>";
print_r($arrTinggi);
echo "</pre>";

arsort($arrTinggi);
reset($arrTinggi);
echo "<b>Array setelah pengurutan dengan arsort()</b>";
echo "<pre>";
print_r($arrTinggi);
echo "</pre>";

ksort($arrTinggi);
reset($arrTinggi);
echo "<b>Array setelah pengurutan dengan ksort()</b>";
echo "<pre>";
print_r($arrTinggi);
echo "</pre>";

krsort($arrTinggi);
reset($arrTinggi);
echo "<b>Array setelah pengurutan dengan krsort()</b>";
echo "<pre>";
print_r($arrTinggi);
echo "</pre>";
?>